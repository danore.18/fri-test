//
//  Meal.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation

struct Meal: Decodable, Hashable {
    var idMeal: String
    var strMeal: String
    var strCategory: String
    var strArea: String
    var strMealThumb: String
    var strYoutube: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(idMeal)
    }
    
    static func == (lhs: Meal, rhs: Meal) -> Bool {
        return lhs.idMeal == rhs.idMeal && lhs.strMeal == rhs.strMeal
    }
}

