//
//  Response.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation

struct Response: Decodable, Hashable {
    var meals: [Meal]
}
