//
//  Api.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation

class Api {
    
    private var session: URLSession
    
    init() {
        session = URLSession(configuration: .default)
    }
    
    func fetch<T: Decodable>(type: T.Type, urlString: String) async throws -> T {
        do  {
            guard let url = URL(string: urlString) else { throw NetworkError.fatalError(description: "Invalid url") }
            var request = URLRequest(url: url)
            request.httpMethod = "get"
            
            let (data, response) = try await session.data(for: request)
            guard let httpResponse = response as? HTTPURLResponse else { throw NetworkError.fatalError(description: "Invalid reponse") }
            
            if httpResponse.statusCode == 200 {
                let jsonDecoder = JSONDecoder()
                return try jsonDecoder.decode(type.self, from: data)
            } else {
                throw NetworkError.fatalError(description: "Bad response")
            }
        } catch {
            throw NetworkError.fatalError(description: error.localizedDescription)
        }
    }

    
}

enum NetworkError: Error {
    case fatalError(description: String)
}
