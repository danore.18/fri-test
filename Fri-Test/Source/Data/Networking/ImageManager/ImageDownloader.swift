//
//  ImageDownloader.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation
import UIKit

enum FetchError: Error{
    case badID
    case badImage
    
}

actor ImageDownloader {
    private enum CacheEntry {
        case inProgress(Task<UIImage?, Error>)
        case ready(UIImage)
    }
    
    private var cache: [URL: CacheEntry] = [:]
    
    private func image(from url: URL) async throws -> UIImage? {
        if let cached = cache[url] {
            switch cached {
                case .ready(let image):
                    return image
                case .inProgress(let handle):
                return try await handle.value
            }
        }
        
        let handle = Task {
            try await downloadImage(from: url)
        }
        
        saveCache(cacheEntry: .inProgress(handle), url: url)
        
        do {
            guard let image = try await handle.value else { return nil }
            saveCache(cacheEntry: .ready(image), url: url)
            return image
        } catch {
            saveCache(cacheEntry: nil, url: url)
            throw error
        }
    }
    
    private func saveCache(cacheEntry: CacheEntry?, url: URL) {
        cache[url] = cacheEntry
    }

    func downloadImage(from url:URL) async throws -> UIImage? {
        let request = URLRequest.init(url:url)
        let (data, response) = try await URLSession.shared.data(for: request)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { throw FetchError.badID }
        let thumbnail = UIImage(data: data)
        return thumbnail
    }
}
