//
//  MealModel.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation

class MealModel {
    var listMeals: Set<Meal> = []
    private let api: Api
    private var counter = 0
    
    init() {
        api = Api()
    }
    
    func fetch() async -> [Meal] {
        repeat {
            do {
                let response = try await api.fetch(type: Response.self, urlString: "https://www.themealdb.com/api/json/v1/1/random.php")
                if let meal = response.meals.first {
                    listMeals.insert(meal)
                    counter += 1
                } else {
                    print("Doesn't work")
                }
            } catch {
                print(error.localizedDescription)
                return []
            }
        } while (listMeals.count <= 19)
        
        return Array(listMeals)
    }
    
}
