//
//  ViewController.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CustomViewCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let model = MealModel()
    private let imageDownloader = ImageDownloader()
    private var listMeals: [Meal] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        
        loadData()
    }
    
    func loadData() {
        Task {
            listMeals = await model.fetch()
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMeals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CustomTableViewCell else { return UITableViewCell() }
        let meal = listMeals[indexPath.row]
        
        cell.title.text = meal.strMeal
        cell.desc.text = "\(meal.strCategory) - \(meal.strArea)"
        cell.delegate = self
        cell.indexPath = indexPath
        
        Task {
            do {
                guard let url = URL(string: meal.strMealThumb) else { return }
                cell.imgView.image = try await imageDownloader.downloadImage(from: url)
            } catch {
                print("error")
            }
        }
        
        return cell
    }

    func goToYouTube(at index: IndexPath) {
        let currentMeal = listMeals[index.row]
        
        if let url = URL(string: currentMeal.strYoutube) {
            UIApplication.shared.open(url)
        }
    }

}

