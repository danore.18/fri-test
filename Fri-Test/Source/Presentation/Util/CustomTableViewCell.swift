//
//  CustomTableViewCell.swift
//  Fri-Test
//
//  Created by Daniel Orellana on 12/02/24.
//

import Foundation
import UIKit

protocol CustomViewCellDelegate: AnyObject {
    func goToYouTube(at index: IndexPath)
}

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    weak var delegate: CustomViewCellDelegate?
    var indexPath: IndexPath!
    
    @IBAction func goYouTube(_ sender: Any) {
        delegate?.goToYouTube(at: indexPath)
    }
    
}
